package lombok.app.com.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat

import com.google.firebase.messaging.FirebaseMessagingService
import lombok.app.com.R
import lombok.app.com.activity.MainActivity


class FireIDService : FirebaseMessagingService() {


    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        sendRegistrationToServer(p0)
    }


//
//
//    override fun onTokenRefresh() {
//
//        val tkn = FirebaseInstanceId.getInstance().token
//        Log.d("token", tkn)
//
//        sendRegistrationToServer(tkn)
//
//
//    }


    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        val pendingIntent = PendingIntent.getActivity(this, 1410 /* Request code */, intent,
                PendingIntent.FLAG_NO_CREATE)
        //       startActivity(intent);

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("FCM Message")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(1410 /* ID of notification */, notificationBuilder.build())
    }
}

