package lombok.app.com.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.tab_kejadian.*
import lombok.app.com.R
import lombok.app.com.activity.TambahLokasiKejadian
import lombok.app.com.adapter.KejadianAdapter
import lombok.app.com.model.DetailLokasi

/**
 * Created by TOSHIBA on 12/07/2018.
 */

class KejadianFragment : Fragment() {

    private var mKejadianList: RecyclerView? = null
    private val adapter = GroupAdapter<ViewHolder>()
    private var mDatabase = FirebaseDatabase.getInstance().reference


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val mView = inflater.inflate(R.layout.tab_kejadian, container, false)
        mKejadianList = mView.findViewById(R.id.kejadian_list)
        mKejadianList?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        mKejadianList?.setHasFixedSize(true)
        mKejadianList?.layoutManager = LinearLayoutManager(context)
        return mView
    }

    override fun onStart() {
        super.onStart()
        mDatabase.keepSynced(true)
        adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        fetchData()
        mDatabase.keepSynced(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDatabase.keepSynced(true)
        swipe_refresh?.setOnRefreshListener {
            fetchData()
        }
        button_add_kejadian?.setOnClickListener {
            startActivity(Intent(activity?.applicationContext, TambahLokasiKejadian::class.java))
        }
        fetchData()
    }


    private fun fetchData() {
        progressBar?.visibility = View.VISIBLE
        mDatabase = FirebaseDatabase.getInstance().reference.child("lokasi_kejadian")
        mDatabase.addListenerForSingleValueEvent(object :ValueEventListener{
            override fun onDataChange(p0: DataSnapshot) {
                adapter.clear()
                p0.children.forEach {
                    val kejadian = it.getValue(DetailLokasi::class.java)
                    if (kejadian != null){
                        adapter.add(KejadianAdapter(kejadian))
                        adapter.notifyDataSetChanged()
                        progressBar?.visibility = View.GONE
                        swipe_refresh.isRefreshing = false
                    }
                }
                kejadian_list.adapter = adapter
                kejadian_list.layoutManager = LinearLayoutManager(activity)
            }
            override fun onCancelled(p0: DatabaseError) {
            }
        })
    }

}
