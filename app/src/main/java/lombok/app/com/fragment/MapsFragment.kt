@file:Suppress("DEPRECATION")

package lombok.app.com.fragment

import android.Manifest.*
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RemoteViews
import android.widget.TextView
import android.widget.Toast

import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import lombok.app.com.R
import lombok.app.com.activity.DetailKejadianActivity
import lombok.app.com.activity.MainActivity
import lombok.app.com.adapter.KejadianAdapter.Companion.EXTRA_DETAIL_KEJADIAN
import lombok.app.com.model.DetailLokasi

@Suppress("DEPRECATION")
class MapsFragment : Fragment(), OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener {

    private var mGoogleMap: GoogleMap? = null
    private var mapView: MapView? = null
    private var mView: View? = null
    private var tkn: String? = null

    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLastLocation: Location? = null
    private var mDatabase: DatabaseReference? = null
    private var lokasiDetail: DatabaseReference? = null
    private var geoFire: GeoFire? = null
    private var mAuth: FirebaseAuth? = null
    private var user_id: String? = null
    private var mProgress: ProgressDialog? = null
    private val marker: Marker? = null
    private var pesan: String? = ""
    internal var lokasiKejadian: DetailLokasi? = null

    lateinit var notifictionManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    lateinit var mBottomSheet: BottomSheetDialog
    private val channelId = "kriminalapp.lukman.com"
    private val description = "TEST NOTIFICATION"
    private lateinit var lokasi: TextView
    private lateinit var jenis: TextView
    private lateinit var tahun: TextView
    private lateinit var jumlah: TextView
    private lateinit var status: TextView
    private lateinit var waktu: TextView
    private lateinit var btnView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        notifictionManager = activity?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (mAuth!!.currentUser != null) {
            user_id = mAuth!!.currentUser!!.uid
            tkn = FirebaseInstanceId.getInstance().token
            Log.d("token", tkn)
        }
    }


    override fun onStart() {
        super.onStart()
        lokasiDetail!!.keepSynced(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mDatabase = FirebaseDatabase.getInstance().reference.child("user_location")
        mProgress = ProgressDialog(context)
        geoFire = GeoFire(mDatabase)
        lokasiDetail = FirebaseDatabase.getInstance().getReference("lokasi_kejadian")
        lokasiDetail!!.keepSynced(true)
        lokasiDetail!!.push().setValue(marker)

    }

    override fun onResume() {
        super.onResume()
        lokasiDetail = FirebaseDatabase.getInstance().getReference("lokasi_kejadian")
        lokasiDetail!!.keepSynced(true)
        lokasiDetail!!.push().setValue(marker)

    }



    @SuppressLint("InflateParams")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.tab_maps, container, false)
        mBottomSheet = BottomSheetDialog(mView!!.context)
        val sheetView = inflater.inflate(R.layout.bottom_sheet_dialog, null)
        mBottomSheet.setContentView(sheetView)
        lokasi = sheetView.findViewById(R.id.nama_lokasi) as TextView
        jenis = sheetView.findViewById(R.id.jenis_kejadian) as TextView
        tahun = sheetView.findViewById(R.id.tahun_kejadian) as TextView
        jumlah = sheetView.findViewById(R.id.jmlh_kejadian) as TextView
        status = sheetView.findViewById(R.id.status_kejaidan) as TextView
        waktu = sheetView.findViewById(R.id.waktu_keajidan) as TextView
        btnView = sheetView.findViewById(R.id.viewButton) as TextView

        return mView
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = with(mView!!) { findViewById(R.id.map) }
        if (mapView != null) {
            mapView!!.onCreate(null)
            mapView!!.onResume()
            mapView!!.getMapAsync(this)
            setUpLocation()
        }
    }

    private fun setUpLocation() {
        if (ActivityCompat.checkSelfPermission(context!!, permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            when {
                ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission.ACCESS_COARSE_LOCATION) -> {

                }
                else -> ActivityCompat.requestPermissions(activity!!, arrayOf(permission.ACCESS_COARSE_LOCATION, permission.ACCESS_FINE_LOCATION), MY_PERMISSION_REQUEST_CODE)
            }
        } else {
            if (checkPlayServices()) {
                buildGoogleApiClient()
                createLocationRequest()
                displayLocation()

            }
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPlayServices()) {
                    buildGoogleApiClient()
                    createLocationRequest()
                    displayLocation()
                }
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun displayLocation() {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
//        mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-8.6821963, 115.9888727), 10.0f))

        if (mLastLocation != null) {
            val latitude = mLastLocation!!.latitude
            val longitude = mLastLocation!!.longitude
            if (mAuth?.currentUser != null) {
                geoFire?.setLocation(user_id!!, GeoLocation(latitude, longitude)) { _, _ -> mGoogleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude, longitude), 11.0f)) }
            }

//                        Toast.makeText(getContext(), String.format("lokasimu adalah" + latitude + longitude), Toast.LENGTH_LONG).show();
            Log.d(TAG, String.format("Lokasimu sudah berubah : %f / %f", latitude, longitude))
        } else {
            Log.d(TAG, "displayLocation: cant get your location")
        }

    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = UPDATE_INTERVAL.toLong()
        mLocationRequest!!.fastestInterval = FATEST_INTERVAL.toLong()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.smallestDisplacement = DISPLACEMENT.toFloat()
    }

    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient!!.connect()
    }

    private fun checkPlayServices(): Boolean {
        val resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context!!)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity, PLAY_SERVICES_RESOULTION_REQUEST).show()
            else {
                Toast.makeText(context, "This device is not supported", Toast.LENGTH_LONG).show()
                finish()
            }
            return false
        }
        return true

    }

    private fun finish() {
        finish()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        MapsInitializer.initialize(context!!)
        mGoogleMap = googleMap
        googleMap.setOnMarkerClickListener(this)
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        if (ActivityCompat.checkSelfPermission(context!!,
                        permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!,
                        permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isCompassEnabled = true
        googleMap.uiSettings.isRotateGesturesEnabled = true
        googleMap.uiSettings.isMapToolbarEnabled = true

        lokasiDetail!!.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                for (s in dataSnapshot.children) {
                    lokasiKejadian = s.getValue<DetailLokasi>(DetailLokasi::class.java)
                    val status = lokasiKejadian?.status
                    var icon: Float?
                    icon = when (status) {
                        "Wisata" -> BitmapDescriptorFactory.HUE_GREEN
                        "Kuliner" -> BitmapDescriptorFactory.HUE_ORANGE
                        else -> BitmapDescriptorFactory.HUE_RED
                    }
                    val lokasi = LatLng(lokasiKejadian?.latitude!!, lokasiKejadian?.longitude!!)
                    mGoogleMap!!.addMarker(MarkerOptions()
                            .position(lokasi)
                            .snippet(lokasiKejadian!!.id)
                            .title(lokasiKejadian!!.nama_lokasi))
                            .setIcon(BitmapDescriptorFactory.defaultMarker(icon))

                    mGoogleMap!!.isTrafficEnabled = true

                    mGoogleMap!!.setOnMarkerClickListener { marker ->
                        mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(marker.position.latitude, marker.position.longitude), 15.0f))
//                        Log.d("MARKER", marker.position.latitude.toString() + marker.position.longitude.toString())
                        ambilData(marker?.snippet)
                        true
                    }
                    mGoogleMap!!.addCircle(CircleOptions()
                            .center(lokasi)
                            .radius(200.0)
                            .strokeColor(Color.BLUE)
                            .fillColor(0x220000ff)
                            .strokeWidth(5.0f)
                    )
                    val geoQuery = geoFire!!.queryAtLocation(GeoLocation(lokasiKejadian?.latitude!!, lokasiKejadian?.longitude!!), 0.2)

                    geoQuery.addGeoQueryEventListener(object : GeoQueryEventListener {
                        override fun onKeyEntered(key: String, geoLocation: GeoLocation) {
//                          if (mAuth!!.currentUser != null) {
                            pesan = "Anda memasuki salah satu object parawisata di lombok, Terima Kasih"
                            sendNotification()
//                            }
                        }
                        override fun onKeyExited(key: String) {
                            if (mAuth!!.currentUser != null) {
                                pesan = "Anda sudah keluar dari salah satu object parawisata di lombok, Terima Kasih"
                                sendNotification()
                            }
                        }
                        override fun onKeyMoved(key: String, lokasiKejadian: GeoLocation) {
                        }

                        override fun onGeoQueryReady() {}
                        override fun onGeoQueryError(error: DatabaseError) {
                            Log.d(TAG, "onGeoQueryError: $error")
                        }
                    })

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    private fun ambilData(idKejadian: String?) {
        val data = FirebaseDatabase.getInstance().reference.child("lokasi_kejadian").child(idKejadian.toString())
        data.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot) {
                val detail = p0.getValue<DetailLokasi>(DetailLokasi::class.java)
                mBottomSheet.show()
                lokasi.text = detail?.nama_lokasi
                jenis.text = detail?.keterangan_lokasi
                tahun.text = detail?.tahun_kejadian
                jumlah.text = detail?.jumlah_kejadian
                status.text = detail?.status
                val mulai = detail?.waktu_mulai.toString()
                val selesai = detail?.waktu_selesai.toString()
                waktu.text = "$mulai - $selesai"
                btnView.setOnClickListener {
                    Intent(it.context, DetailKejadianActivity::class.java).apply {
                        putExtra(EXTRA_DETAIL_KEJADIAN, detail)
                        startActivity(this)
                    }
                }

            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun sendNotification() {
        val intent = Intent(activity, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val contentView = RemoteViews(activity?.packageName, R.layout.notification_layout)
        contentView.setTextViewText(R.id.tv_content, pesan)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notifictionManager.createNotificationChannel(notificationChannel)
            builder = Notification.Builder(activity, channelId)
                    .setContent(contentView)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setLargeIcon(BitmapFactory.decodeResource(activity?.resources, R.drawable.ic_launcher))
                    .setContentIntent(pendingIntent)
        } else {
            builder = Notification.Builder(activity)
                    .setContent(contentView)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setLargeIcon(BitmapFactory.decodeResource(activity?.resources, R.drawable.ic_launcher))
                    .setContentIntent(pendingIntent)
        }
        notifictionManager.notify(1234, builder.build())
    }

    override fun onConnected(bundle: Bundle?) {
        displayLocation()
        startLocationUpdates()
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context!!,
                        permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.FusedLocationApi?.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)

    }

    override fun onConnectionSuspended(i: Int) {
        mGoogleApiClient!!.connect()

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    override fun onLocationChanged(location: Location) {
        mLastLocation = location
        displayLocation()
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        marker.hideInfoWindow()
        return true
    }


    companion object {
        private const val MY_PERMISSION_REQUEST_CODE = 7192
        private const val PLAY_SERVICES_RESOULTION_REQUEST = 300193
        private const val UPDATE_INTERVAL = 5000 //
        private const val FATEST_INTERVAL = 3000 //
        private const val DISPLACEMENT = 10 //
        private const val TAG = "MapsFragment"
    }
}


