package lombok.app.com.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import lombok.app.com.model.DetailLokasi


@Dao
interface LokasiDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
     suspend fun insert(lokasi : DetailLokasi) : Long

    @Query("SELECT * FROM DetailLokasi " )
    fun getLokasi() : LiveData<List<DetailLokasi>>?

    @Query("DELETE FROM detaillokasi WHERE id = :id ")
    suspend fun deleteLokasi(id : String)


}