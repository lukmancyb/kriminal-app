package lombok.app.com.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import lombok.app.com.database.dao.LokasiDao
import lombok.app.com.model.DetailLokasi

@Database(
        exportSchema = false,
        entities = [DetailLokasi::class],
        version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun lokasiDao(): LokasiDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "MyDatabase.db"
                ).build()

    }
}