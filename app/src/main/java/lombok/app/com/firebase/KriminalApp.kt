package lombok.app.com.firebase

import android.app.Application

import com.google.firebase.database.FirebaseDatabase

/**
 * Created by TOSHIBA on 16/07/2018.
 */

class KriminalApp : Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }
}
