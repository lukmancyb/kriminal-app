package lombok.app.com.utils

import android.app.AlertDialog
import android.content.Context


fun Context.showDialog(msg: String): Boolean {

    var flag : Boolean = false

    AlertDialog.Builder(this).apply {
        setTitle("Action")
        setMessage(msg)
        setPositiveButton("OK") { _, _ ->
            flag = true
        }
        setNeutralButton("Cancel") { dialog, _ ->
            flag = false
            dialog.dismiss()
        }
        create()
        show()
        return flag
    }


}