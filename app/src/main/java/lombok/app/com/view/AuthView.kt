package lombok.app.com.view

interface AuthView{

    fun showSuccessResponse(){
    }

    fun showErrorResponse(){

    }

    fun showLoading(){}
    fun hideLoading(){}
}