package lombok.app.com.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

/**
 * Created by TOSHIBA on 15/07/2018.
 */


@Parcelize
@Entity
data class DetailLokasi(
        @PrimaryKey(autoGenerate = false) val id: String = "",
        val nama_lokasi: String? = "",
        val status: String? = null,
        val keterangan_lokasi: String? = null,
        val tahun_kejadian: String? = null,
        val waktu_mulai: String? = null,
        val waktu_selesai: String? = null,
        val tanggal_kejadian: String? = null,
        val jumlah_kejadian: String? = null,
        val latitude: Double? = 0.toDouble(),
        val longitude: Double? = 0.toDouble(),
        val gambar: String? = null) : Parcelable


