package lombok.app.com.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by #PemimpinMuda on 10/26/2018.
 */
@Parcelize
data class Users(val uid : String,
                 val nama : String,
                 val userImageUrl : String,
                 val telepon : String ) : Parcelable{
    constructor() : this("","","","")
}