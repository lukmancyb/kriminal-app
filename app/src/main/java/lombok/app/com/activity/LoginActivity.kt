@file:Suppress("DEPRECATION")

package lombok.app.com.activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_login_page.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.yesButton
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.Task
import lombok.app.com.R
import lombok.app.com.presenter.AuthPresenter
import lombok.app.com.view.AuthView

@Suppress("DEPRECATION")
class LoginActivity : Activity(), AuthView {
    private var mDatabaseUsers: DatabaseReference? = null
    private var mAuth: FirebaseAuth? = null
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var mAuthPresenter : AuthPresenter
    private lateinit var progressDialog : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_page)
        mDatabaseUsers = FirebaseDatabase.getInstance().reference.child("users")
        mAuth = FirebaseAuth.getInstance()
        mAuthPresenter = AuthPresenter(this)
        progressDialog =  indeterminateProgressDialog("Please wait...")
        progressDialog.hide()

//        mLoginBtn = findViewById(R.id.loginBtn)
        configureGoogleSignin()
        loginBtn.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                    val email = emailField.text.toString().trim()
                    val password = passwordField.text.toString().trim()
                    if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                        alert("Masukan Username dan Password terlebih dahulu . .") {
                            yesButton {
                            }
                        }.show()
                    } else {
                        prosesLogin()
                    }
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }

        googleLogin.setOnClickListener {
            googleSign()
        }
        daftar.setOnClickListener {
            val i = Intent(this@LoginActivity, RegistrasiActivity::class.java)
            startActivity(i)
        }
    }

    private fun prosesLogin() {
        val email = emailField.text.toString().trim()
        val password = passwordField.text.toString().trim()
        Log.d(TAG, email + "" + password)
        if (isNetworkAvailable) {
            if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
//                val progress = indeterminateProgressDialog("Please wait...")
//                progress.show()
                showLoading()
                mAuth!!.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        checkUserExist()
                    } else {
                        hideLoading()
                        alert("Username dan password tidak terdaftar") {
                            yesButton {
                                passwordField.text.clear()
                            }
                        }.show()
                    }
                }
            }
        }
    }

    private fun checkUserExist() {
        val userId = mAuth!!.currentUser!!.uid
        mDatabaseUsers!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.hasChild(userId)) {
                    toMainActivity()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })

    }

    companion object {
        private const val TAG = "LoginActivity"
        private const val RC_SIGN_IN = 1
    }

    private val isNetworkAvailable: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

    private fun googleSign() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
                mAuthPresenter.getFirebaseAuthGoogle(task)
                showLoading()
                Log.d("TASK", task.isSuccessful.toString())
            }
        }
    }


    private fun toMainActivity() {
        val mainIntent = Intent(this@LoginActivity, MainActivity::class.java)
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(mainIntent)
        finish()
    }

    private fun configureGoogleSignin() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, mGoogleSignInOptions)
    }

    override fun showSuccessResponse() {
        super.showSuccessResponse()
        hideLoading()
        toMainActivity()
    }

    override fun showErrorResponse() {
        super.showErrorResponse()
        hideLoading()
        Toast.makeText(this, "Gagal login silahkan check jaringan internet atau update Play Service Anda", Toast.LENGTH_LONG).show()

    }


    override fun showLoading() {
                progressDialog.show()
    }

    override fun hideLoading() {
                progressDialog.dismiss()
    }

}
