@file:Suppress("DEPRECATION")

package lombok.app.com.activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.widget.EditText
import android.widget.Toast

import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_registrasi_page.*
import lombok.app.com.R
import org.jetbrains.anko.*


class RegistrasiActivity : AppCompatActivity() {


    private var mNama: EditText? = null
    private var mEmail: EditText? = null
    private var mPassword: EditText? = null
    private var mAuth: FirebaseAuth? = null

    private var mProgressDialog: ProgressDialog? = null
    private val user_id: String? = null


    //cek koneksi internet
    private val isNetworkAvailable: Boolean
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrasi_page)
        mNama = findViewById(R.id.namaField)
        mEmail = findViewById(R.id.emailField)
        mPassword = findViewById(R.id.passwordField)

        mAuth = FirebaseAuth.getInstance()
        mProgressDialog = ProgressDialog(this)

        registerBtn.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    v.background.setColorFilter(-0x1f0b8adf, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP -> {
                    v.background.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        }


        registerBtn.setOnClickListener {
//            registerBtn.setBackgroundColor(resources.getColor(R.color.transparan))
            if (isNetworkAvailable) {

                prosesRegistrasi()
            } else {
                Toast.makeText(this@RegistrasiActivity, "Anda tidak terhubung Internet", Toast.LENGTH_LONG).show()

            }
        }
    }

    private fun prosesRegistrasi() {
        val nama = mNama!!.text.toString()
        val email = mEmail!!.text.toString()
        val password = mPassword!!.text.toString()

        if (!TextUtils.isEmpty(nama) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            val progress = indeterminateProgressDialog("Please wait...")
            progress.show()
            Log.d("DATA", nama + email + password)
            mAuth!!.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    progress.dismiss()
                    val mainIntent = Intent(this@RegistrasiActivity, MainActivity::class.java)
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(mainIntent)

                }
            }
        }else{
            alert("Silahkan Megisi Data anda terlebih dahulu"){
                yesButton {  }
            }.show()
        }
    }

}
