package lombok.app.com.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_kejadian.*
import lombok.app.com.R
import lombok.app.com.adapter.KejadianAdapter.Companion.EXTRA_DETAIL_KEJADIAN
import lombok.app.com.database.AppDatabase
import lombok.app.com.model.DetailLokasi
import lombok.app.com.utils.Coroutines

class DetailKejadianActivity : AppCompatActivity() {

    companion object {
        const val TAG = "DETAILKEJADIAN"
    }

    private lateinit var db: AppDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kejadian)
        db = AppDatabase.invoke(this.applicationContext)

        val dataExtra = intent.getParcelableExtra<DetailLokasi>(EXTRA_DETAIL_KEJADIAN)
        Log.d(TAG, dataExtra.toString())

        setupUi(dataExtra)
    }

    @SuppressLint("SetTextI18n")
    private fun setupUi(dataExtra: DetailLokasi?) {

        d("GAMBAR", dataExtra?.gambar.toString())

        if (dataExtra?.gambar != null) {
            Picasso.get().load(dataExtra.gambar).into(image_detail_kejadian)
        } else {
            image_detail_kejadian.setImageResource(R.drawable.select_pictures)
        }

        text_lokasi_kejadian?.text = if(dataExtra?.nama_lokasi.isNullOrEmpty()) "No Address" else dataExtra?.nama_lokasi
        text_jumlah_kejadian?.text = dataExtra?.jumlah_kejadian
        text_ranggal_kejadian?.text = dataExtra?.tanggal_kejadian
        text_jam?.text = "${dataExtra?.waktu_mulai} - ${dataExtra?.waktu_selesai}"
        text_urgency?.text = dataExtra?.status

        button_add_list.setOnClickListener {
            Coroutines.main {
                db.lokasiDao().insert(dataExtra!!)
                Snackbar.make(button_add_list, "${dataExtra.keterangan_lokasi} sudah ditambahkan", Snackbar.LENGTH_LONG).show()

            }
        }
    }
}
