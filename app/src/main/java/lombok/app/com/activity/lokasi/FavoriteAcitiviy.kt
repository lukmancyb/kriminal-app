package lombok.app.com.activity.lokasi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.activity_favorite_acitiviy.*
import lombok.app.com.R
import lombok.app.com.database.AppDatabase
import lombok.app.com.model.DetailLokasi

class FavoriteAcitiviy : AppCompatActivity(), ItemFavorite.OnPressItem {


    private lateinit var db: AppDatabase
    private lateinit var factory: FavoriteViewModelFactory
    private lateinit var viewmodel: FavoriteViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_acitiviy)

        db = AppDatabase.invoke(this.applicationContext)
        factory = FavoriteViewModelFactory(db)
        viewmodel = ViewModelProviders.of(this, factory).get(FavoriteViewModel::class.java)

        viewmodel.getLokasi()?.observe(this, Observer {
            if (it.isNullOrEmpty()) {
                noDataLayout.visibility = View.VISIBLE
                info.visibility = View.GONE
                initRecyclerView(it)
            } else {
                initRecyclerView(it)
                info.visibility = View.VISIBLE
                noDataLayout.visibility = View.GONE
            }
        })


    }

    private fun initRecyclerView(item: List<DetailLokasi>) {
        rvFavorite.also { rv ->
            val adapter = GroupAdapter<ViewHolder>().apply {
                addAll(item.toListItem())
                notifyDataSetChanged()
            }
            rv.layoutManager = LinearLayoutManager(this)
            rv.adapter = adapter
        }

    }

    private fun List<DetailLokasi>.toListItem(): List<ItemFavorite> {
        return map {
            ItemFavorite(it, this@FavoriteAcitiviy)
        }
    }

    override fun onItemPressed(item: Item, data: DetailLokasi) {
        showDialog(data)

    }

    private fun showDialog(data: DetailLokasi) {


        AlertDialog.Builder(this).apply {
            setTitle("Action")
            setMessage("Yakin ingin menghapus data ini")
            setPositiveButton("OK") { _, _ ->
                viewmodel.deleteLokasi(data.id)
            }
            setNeutralButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            create()
            show()
        }


    }
}
