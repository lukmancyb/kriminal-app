package lombok.app.com.activity.lokasi

import androidx.lifecycle.ViewModel
import lombok.app.com.database.AppDatabase
import lombok.app.com.utils.Coroutines

class FavoriteViewModel(
        private val db : AppDatabase
) : ViewModel() {



    fun getLokasi() = db.lokasiDao().getLokasi()

    fun deleteLokasi(id : String){
        Coroutines.main {
            db.lokasiDao().deleteLokasi(id)
        }
    }
}