package lombok.app.com.activity.lokasi

import android.content.Intent
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_favorite.view.*
import lombok.app.com.R
import lombok.app.com.activity.DetailKejadianActivity
import lombok.app.com.adapter.KejadianAdapter.Companion.EXTRA_DETAIL_KEJADIAN
import lombok.app.com.model.DetailLokasi


class ItemFavorite(
        val data : DetailLokasi, private val listener : OnPressItem
) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
       viewHolder.itemView.apply {
           txtJudul.text = data.keterangan_lokasi
           txtDescription.text = data.status
           setOnClickListener {
               val intent = Intent(viewHolder.itemView.context, DetailKejadianActivity::class.java)
               intent.putExtra(EXTRA_DETAIL_KEJADIAN, data)
               viewHolder.itemView.context.startActivity(intent)
           }
           setOnLongClickListener{
               listener.onItemPressed(this@ItemFavorite, data)
               true
           }
       }
    }

    override fun getLayout() = R.layout.item_favorite

    interface OnPressItem {
        fun onItemPressed(item: Item, data : DetailLokasi )
    }
}