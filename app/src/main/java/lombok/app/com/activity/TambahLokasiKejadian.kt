package lombok.app.com.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_add_lokasi_kejadian.*
import lombok.app.com.R
import lombok.app.com.model.DetailLokasi
import lombok.app.com.presenter.GeocoderPresenter
import lombok.app.com.view.GeocoderView
import pub.devrel.easypermissions.EasyPermissions
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class TambahLokasiKejadian : AppCompatActivity(), GeocoderView {

    private lateinit var geopresenter: GeocoderPresenter
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private var lat: Double? = 0.toDouble()
    private var lng: Double? = 0.toDouble()
    private var status: String? = ""
    private var lokasi: String? = ""
    private var timeStart: String? = ""
    private var timeEnd: String? = ""
    private var kategori: String? = ""
    private var jumlah: String? = ""
    private var imageUri: Uri? = null
    private var imageName: String? = "https://firebasestorage.googleapis.com/v0/b/kriminalapp-31dc8.appspot.com/o/images%2FnoImage.jpg?alt=media&token=2c0f9a7d-4e87-4570-b525-97a205b8d0f2"


    companion object {
        private const val REQUEST_LOKASI_CODE = 113
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_lokasi_kejadian)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        geopresenter = GeocoderPresenter(this)
        button_camera?.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        button_time_start?.setOnClickListener {
            getTimePicker("start")
        }

        button_time_end?.setOnClickListener {
            getTimePicker("end")
        }

        button_add_lokasi?.setOnClickListener {
            showLoading()
            editText_lokasi_kejadian.setText("Getting location . .")
            requestLocationPermission()

        }

        close_image?.setOnClickListener {
            imageUri = null
            card_image?.visibility = View.GONE
        }

        val itemSpinner = arrayOf("Wisata", "Kuliner", "Lainnya")
        val spinerAdapter = ArrayAdapter(this, R.layout.spiner_text, itemSpinner)
        spiner_menu?.adapter = spinerAdapter
        spinerAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown)
        spiner_menu.setSelection(0)
        spiner_menu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                status = itemSpinner[position]

                Log.d("SPINEER", status)
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.add_aduan_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_send -> {
                showLoading()
//                uploadDataIntoFirebase()
                uploadData()
            }
        }
        return super.onOptionsItemSelected(item)

    }

//    private fun uploadDataIntoFirebase() {
//        uploadData()
//    }

    @SuppressLint("MissingPermission")
    private fun requestLocationPermission() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            mFusedLocationProviderClient.lastLocation.addOnSuccessListener(this) {
                lat = it?.latitude
                lng = it?.longitude
                if (lat != null || lng != null) {
                    geopresenter.getLocationName(lat = it?.latitude, long = it?.longitude)
                }
            }
        } else {
            EasyPermissions.requestPermissions(
                    this, "Aplikasi membutuhkan akses untuk mengetahui lokasi anda",
                    REQUEST_LOKASI_CODE, Manifest.permission.ACCESS_FINE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOKASI_CODE -> requestLocationPermission()
        }
    }

    private fun uploadImageToFirebase() {
        if (imageUri != null) {
            val filename = UUID.randomUUID().toString()
            val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
            ref.putFile(imageUri!!)
                    .addOnSuccessListener {
                        ref.downloadUrl.addOnSuccessListener {
                            saveDataToFirebase(it.toString())
                        }
                    }
                    .addOnFailureListener {
                        //do something
                    }
        } else {
            saveDataToFirebase(imageName!!)
        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun saveDataToFirebase(img: String) {
        imageName = img
        val tanggal = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
            current.format(formatter)
        } else {
            val date = Date()
            val formatter = SimpleDateFormat("dd/MM/yyyy")
            formatter.format(date)
        }
        val years = Calendar.getInstance().get(Calendar.YEAR).toString()
        val idLokasi = UUID.randomUUID().toString()
        val ref = FirebaseDatabase.getInstance().getReference("/lokasi_kejadian/$idLokasi")
        val data = DetailLokasi(id = idLokasi,
                nama_lokasi = lokasi,
                status = status,
                keterangan_lokasi = kategori,
                tahun_kejadian = years,
                waktu_mulai = timeStart,
                waktu_selesai = timeEnd,
                tanggal_kejadian = tanggal,
                jumlah_kejadian = jumlah,
                latitude = lat,
                longitude = lng,
                gambar = imageName
        )
        ref.setValue(data)
                .addOnSuccessListener {
                    hideLoading()
                    Snackbar.make(editText_lokasi_kejadian, "Success uploaded", Snackbar.LENGTH_LONG).show()
                    finish()
                }
                .addOnFailureListener {

                }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
            image_view?.setImageBitmap(bitmap)
            card_image.visibility = View.VISIBLE
        }
    }


    @SuppressLint("SetTextI18n")
    private fun getTimePicker(code: String) {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR)
        val minute = c.get(Calendar.MINUTE)

        val timePickerDialog = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener(function = { _, h, m ->
            when (code) {
                "start" -> {
                    editText_time_start?.setText("$h:$m")
                    editText_time_start?.error = null
                }
                "end" -> {
                    editText_time_end?.setText("$h:$m")
                    editText_time_end?.error = null
                }
            }
        }), hour, minute, true)
        timePickerDialog.show()
    }

    override fun showLocationName(data: String?) {
        data?.let {
            editText_lokasi_kejadian.setText(it)
            hideLoading()
        }
    }

    override fun notFound() {
        super.notFound()
        editText_lokasi_kejadian.hint = "Lokasi tidak ditemukan"
        Snackbar.make(editText_lokasi_kejadian, "Lokasi tidak ditemukan", Snackbar.LENGTH_LONG).show()
    }

    private fun uploadData() {
        when {
            editText_jumlah?.text.isNullOrBlank() -> editText_jumlah?.error = "Isian tidak boleh kosong"
            editText_kategori_kriminalitas?.text.isNullOrBlank() -> editText_kategori_kriminalitas?.error = "Isian tidak boleh kosong"
            editText_time_start?.text.isNullOrBlank() -> editText_time_start?.error = "Isian tidak boleh kosong"
            editText_time_end?.text.isNullOrBlank() -> editText_time_end?.error = "Isian tidak boleh kosong"
            else -> {
                lokasi = editText_lokasi_kejadian.text.toString()
                jumlah = editText_jumlah?.text.toString()
                kategori = editText_kategori_kriminalitas?.text.toString()
                timeStart = editText_time_start.text.toString()
                timeEnd = editText_time_end.text.toString()
                uploadImageToFirebase()
            }
        }


    }

    private fun showLoading() {
        disable_layout?.visibility = View.VISIBLE
        progressbar?.visibility = View.VISIBLE

    }

    private fun hideLoading() {
        disable_layout?.visibility = View.GONE
        progressbar?.visibility = View.GONE
    }
}
