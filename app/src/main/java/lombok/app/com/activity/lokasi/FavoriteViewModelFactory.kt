package lombok.app.com.activity.lokasi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import lombok.app.com.database.AppDatabase

@Suppress("UNCHECKED_CAST")
class FavoriteViewModelFactory(
        private val db : AppDatabase
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FavoriteViewModel(db) as T
    }
}