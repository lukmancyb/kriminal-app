package lombok.app.com.presenter

import android.util.Log
import com.mapbox.api.geocoding.v5.GeocodingCriteria
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.geojson.Point
import lombok.app.com.utils.YOUR_MAPBOX_ACCESS_TOKEN
import lombok.app.com.view.GeocoderView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class GeocoderPresenter(private val geocoderView: GeocoderView){

    fun getLocationName(lat : Double?, long : Double?){

        val reverseGeocode = MapboxGeocoding.builder()
            .accessToken(YOUR_MAPBOX_ACCESS_TOKEN)
            .query(Point.fromLngLat(long!!,lat!!))
            .geocodingTypes(GeocodingCriteria.TYPE_NEIGHBORHOOD)
            .build()

        reverseGeocode.enqueueCall(object : Callback<GeocodingResponse>{
            override fun onResponse(call: Call<GeocodingResponse>, response: Response<GeocodingResponse>) {
                Log.d("GOE", response.body().toString())
                val results = response.body()!!.features()
                if (results.size > 0){
                    val firstResultPoint = results[0].placeName()
                    geocoderView.showLocationName(firstResultPoint)
                }else{
                    geocoderView.notFound()
                }
            }

            override fun onFailure(call: Call<GeocodingResponse>, t: Throwable) {
                t.printStackTrace()
            }


        })
    }


}