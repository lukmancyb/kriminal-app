package lombok.app.com.presenter

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import lombok.app.com.view.AuthView

class AuthPresenter (val view : AuthView){

    private var mAuth = FirebaseAuth.getInstance()
    fun getFirebaseAuthGoogle(task: Task<GoogleSignInAccount>){
        if (task.isSuccessful){
            try {
                val account = task.getResult(ApiException::class.java)
                val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
                mAuth.signInWithCredential(credential).addOnCompleteListener { it ->
                    if (it.isSuccessful) {
                        view.showSuccessResponse()
                    }
                }
            }catch (a : ApiException){

            }
        }else{
            view.showErrorResponse()
        }

    }
}