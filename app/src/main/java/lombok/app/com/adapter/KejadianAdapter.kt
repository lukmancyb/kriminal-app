package lombok.app.com.adapter

import android.content.Intent
import com.squareup.picasso.Picasso
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.kejadian_row.view.*
import lombok.app.com.R
import lombok.app.com.activity.DetailKejadianActivity
import lombok.app.com.model.DetailLokasi


class KejadianAdapter(private val kejadian: DetailLokasi) : Item<ViewHolder>() {

    companion object {
        const val EXTRA_DETAIL_KEJADIAN = "EXTRA_DETAIL_KEJADIAN"
    }
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.judul_kejadian.text = kejadian.keterangan_lokasi
        viewHolder.itemView.lokasi_kejadian.text = if(kejadian.nama_lokasi.isNullOrEmpty()) "No Address" else kejadian.nama_lokasi
        viewHolder.itemView.tanggal_kejadian.text = kejadian.tanggal_kejadian
        Picasso.get().load(kejadian.gambar).into(viewHolder.itemView.imgKejadian)
        viewHolder.itemView.card_item.setOnClickListener {
           val intent = Intent(viewHolder.itemView.context, DetailKejadianActivity::class.java)
            intent.putExtra(EXTRA_DETAIL_KEJADIAN, kejadian)
            viewHolder.itemView.context.startActivity(intent)
        }
    }

    override fun getLayout(): Int {
        return R.layout.kejadian_row
    }

}